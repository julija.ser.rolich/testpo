import {goto, run, stop} from "../lib/browserFirefox";
import {url} from "../framework/config/url";
import {app} from "../framework/pages";
import chai from 'chai';
const {expect} = chai;

describe.skip('FIREFOX Авторизация и навигация по главному меню, desktop версия', () => {
    let page;
    beforeEach(async () => {
        await run();
        page = await goto(url.idemobspb);
        await page.waitForSelector('.container > #login-container > #login-form > .secondary-links > .locale');
        await page.click('.container > #login-container > #login-form > .secondary-links > .locale');
    });
    afterEach(async () => {
        await stop();
    });


    it('FIREFOX  Пользователь может ввести логин, пароль и авторизоваться', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        const userGreeting = await page.textContent('.content > #controls > #user #user-greeting');

        expect('Hello World!').to.have.string(userGreeting);
    });

    it('FIREFOX Пользователь видит pop-up с информацией, по нажатию кнопки Восстановить доступ', async () => {
        await app().AuthPage().forget(page);
        const resetDialog = await page.textContent('#reset-password-dialog > .modal-dialog > .modal-content > .modal-header > h3');

        expect('Забыли логин или пароль?').to.have.string(resetDialog);
    });


});
