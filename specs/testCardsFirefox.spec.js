import {goto, run, stop} from "../lib/browserFirefox";
import {url} from "../framework/config/url";
import {app} from "../framework/pages";
import {MenuFragments} from "../framework/page fragments/menuFragments";
import chai from 'chai';
const {expect} = chai;

describe.skip('FIREFOX Заказ карт ', () => {
    let page;
    beforeEach(async () => {
        await run();
        page = await goto(url.idemobspb);
        await page.setViewportSize({ width: 1830, height: 487 });
        await page.waitForSelector('.container > #login-container > #login-form > .secondary-links > .locale');
        await page.click('.container > #login-container > #login-form > .secondary-links > .locale');

    });
    afterEach(async () => {
        await stop();
    });

    it('FIREFOX Карты: пользователь может инициировать заказ карт и заполнить форму заказа карты типа Яркая', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await new MenuFragments().gotoPageCards(page);
        await app().CardsPage().newCard(page);
        await app().CardsPage().openDropDownCards(page);
        //выбор и заказ новой карты типа "Яркая" (с заполнением поля Ежемесячный доход):
        await page.selectOption('#cards-vertical-container #type-select', 'YARKO');
        const buttonOrderYarko = '#card-85 > .row-fluid > .span7 > .row-fluid > .btn';
        await page.waitForSelector(buttonOrderYarko);
        await page.click(buttonOrderYarko);
        const fieldMonthlyIncome = '#application-inputs > .control-group > .controls > .input-append > .input';
        await page.waitForSelector(fieldMonthlyIncome);
        await page.click(fieldMonthlyIncome);
        await page.fill(fieldMonthlyIncome, '1000');
        //включение всех чек-боксов согласий:
        await page.waitForSelector('.control-group:nth-child(1) > .controls > div > .checkbox > .immune');
        await page.click('.control-group:nth-child(1) > .controls > div > .checkbox > .immune');
        await page.waitForSelector('.control-group:nth-child(2) > .controls > div > .checkbox > .immune');
        await page.click('.control-group:nth-child(2) > .controls > div > .checkbox > .immune');
        await page.waitForSelector('.control-group:nth-child(3) > .controls > div > .checkbox > .immune');
        await page.click('.control-group:nth-child(3) > .controls > div > .checkbox > .immune');
        await page.waitForSelector('.control-group:nth-child(4) > .controls > div > .checkbox > .immune');
        await page.click('.control-group:nth-child(4) > .controls > div > .checkbox > .immune');

        const confirmButton = await page.textContent('#contentbar > #loan-application-form #inspect');

        expect(confirmButton).to.have.string('Отправить заявку');
    });

});
