import {goto, run, stop} from "../lib/browser";
import {url} from "../framework/config/url";
import {app} from "../framework/pages";
import chai from 'chai';
const {expect} = chai;

describe('Авторизация в приложении, desktop версия', () => {
    let page;
    beforeEach(async () => {
        await run();
        page = await goto(url.idemobspb);
    });
    afterEach(async () => {
        await stop();
    });


    it('Пользователь может ввести логин, пароль и авторизоваться', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        const userGreeting = await page.textContent('.content > #controls > #user #user-greeting');

        expect('Hello World!').to.have.string(userGreeting);
    });

    it('Пользователь видит pop-up с информацией, по нажатию кнопки Восстановить доступ', async () => {
        await app().AuthPage().forget(page);
       const resetDialog = await page.textContent('#reset-password-dialog > .modal-dialog > .modal-content > .modal-header > h3');

        expect('Забыли логин или пароль?').to.have.string(resetDialog);
    });


});
