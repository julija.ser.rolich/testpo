import {goto, run, stop} from "../lib/browser";
import {url} from "../framework/config/url";
import {app} from "../framework/pages";
import chai from 'chai';
const {expect} = chai;

describe.skip('Выход из приложения на страницу авторизации', () => {
    let page;
    beforeEach(async () => {
        await run();
        page = await goto(url.idemobspb);
    });
    afterEach(async () => {
        await stop();
    });

    it('Пользователь может выйти из интерент-банкинга, по нажатию на иконку "крестик" в верхнем меню сайта ', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await app().Logout().logout(page);
        const loginButton = await page.textContent('.container > #login-container > #login-form #login-button');
        expect(loginButton).to.have.string('Войти');
    });

});

