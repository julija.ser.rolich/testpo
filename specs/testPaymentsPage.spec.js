import {goto, run, stop} from "../lib/browser";
import {url} from "../framework/config/url";
import {app} from "../framework/pages";
import chai from 'chai';
const {expect} = chai;

describe.skip('Навигация внутри вкладки Платежи + наличие транзикций в Истории', () => {
    let page;
    beforeEach(async () => {
        await run();
        page = await goto(url.idemobspb);
    });
    afterEach(async () => {
        await stop();
    });

    it('Навигация в Платежах: пользователь может перейти в подраздел История', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await  app().PaymentsPage().goPayments(page);
        await app().PaymentsPage().history(page);
        const historyHeader = await page.textContent('#contentbar > .tab-content > .page-header > h3 > small');

        expect(historyHeader).to.have.string('операций в системе');
    });

    it('Навигация в Платежах: пользователь может перейти в подраздел Избранное', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await app().PaymentsPage().goPayments(page);
        await app().PaymentsPage().favorites(page);
        const favoritesHeader = await page.textContent('#body > #contentbar > .tab-content > .page-header > h3');

        expect(favoritesHeader).to.have.string('Избранное');
    });

    it('Навигация в Платежах: пользователь может перейти в подраздел Переводы', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await app().PaymentsPage().goPayments(page);
        await app().PaymentsPage().transfers(page);
        const transfersHeader = await page.textContent('#body > #contentbar > .tab-content > .page-header > h3');

        expect(transfersHeader).to.have.string('Переводы');
    });

    it('Навигация в Платежах: пользователь может нажать на пункт подраздела По телефону в другой банк', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await app().PaymentsPage().goPayments(page);
        const fastPaymentsIcon = await page.textContent('#contentbar > #payments-left-menu > #dashboard-payment-type-menu > li > .fast-payments');

        expect(fastPaymentsIcon).to.have.string('По телефону в другой банк');
    });

    it('Навигация в Платежах: пользователь может перейти в подраздел Оплата услуг', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await app().PaymentsPage().goPayments(page);
        await app().PaymentsPage().vendorPayments(page);
        const vendorPaymentsHeader = await page.textContent('#body > #contentbar > .tab-content > .page-header > h3');

        expect(vendorPaymentsHeader).to.have.string('Оплата услуг');
    });

    it('Навигация в Платежах: пользователь может перейти в подраздел Автоплатежи и подписки', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await app().PaymentsPage().goPayments(page);
        await app().PaymentsPage().autoPayment(page);
        const autoPaymentHeader = await page.textContent('#body > #contentbar > .tab-content > .page-header > h3');

        expect(autoPaymentHeader).to.have.string('Автоплатежи и подписки');
    });

    it('Навигация в Платежах: пользователь может перейти в подраздел Запрос перевода', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await app().PaymentsPage().goPayments(page);
        await app().PaymentsPage().requestMoney(page);
        const requestMoneyHeader = await page.textContent('#body > #contentbar > .tab-content > .page-header > h3');

        expect(requestMoneyHeader).to.have.string('Создать запрос на перевод с карты');
    });

    it('Навигация в Платежах: пользователь может нажать на пункт подраздела Мои балансы', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await app().PaymentsPage().goPayments(page);
        const myBalancesIcon = await page.textContent('#contentbar > #payments-left-menu > #dashboard-payment-type-menu #krawlly');

        expect(myBalancesIcon).to.have.string('Мои балансы');
    });

    it('Платежи: пользователь видит таблицу с транзакциями в подразделе История (заголовок Статус), после выбора счета Зарплатный', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await app().PaymentsPage().goPayments(page);
        await app().PaymentsPage().history(page);
        await app().PaymentsPage().dropDownAllBills(page);
        //выбор значения "Зарплатный" из drop-down "Все счета":
        await page.selectOption('#contentbar > .tab-content > #history-form > #payment-history-filter > select', '10028');
        await app().PaymentsPage().datePickerFrom(page);
        //выбор начальной даты (01 число текущего месяца):
        await page.waitForSelector('.datepicker:nth-child(18) > .datepicker-days > .table-condensed > tbody > tr:nth-child(1) > .day:nth-child(2)');
        await page.click('.datepicker:nth-child(18) > .datepicker-days > .table-condensed > tbody > tr:nth-child(1) > .day:nth-child(2)');
        await app().PaymentsPage().buttonApply(page);
        //элемент таблицы платежей, столбец "Статус":
        const elTablePayment = ('//th[contains(text(),\'Статус\')]');
        await page.waitForSelector(elTablePayment);
        await page.click(elTablePayment);
        await page.textContent(elTablePayment);

        expect(elTablePayment).to.have.string('Статус');
    });

});
