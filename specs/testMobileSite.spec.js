import {goto, run, stop} from "../lib/browser";
import {url} from "../framework/config/url";
import {app} from "../framework/pages";
import {MenuFragments} from "../framework/page fragments/menuFragments";
import chai from 'chai';
const {expect} = chai;


describe.skip('Mobile сьюит: авторизация и навигация по главному меню, адаптивная версия сайта', () => {
    let page;
    beforeEach(async () => {
        await run();
        page = await goto(url.idemobspb);
        await page.setViewportSize({ width: 320, height: 600 })

    });
    afterEach(async () => {
        await stop();
    });


    it('Первый тест, пользователь может ввести логин, пароль и авторизоваться в адаптивной версии сайта', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        const userGreeting = await page.textContent('.content > #controls > #user #user-greeting');

        expect('Hello World!').to.have.string(userGreeting);
    });

    it('Главное меню: пользователь может перети на вкладку Обзор в адаптивной версии сайта', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await new MenuFragments().gotoPageOverview(page);
        const overviewPageHeader = await page.textContent('#body > #contentbar > #header-container > .page-header > h1');

        expect(overviewPageHeader).to.have.string('Обзор');
    });

    it('Главное меню: пользователь может перейти на вкладку Счета и инициировать открытие нового счета в адаптивной версии сайта', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await new MenuFragments().gotoPageAccounts(page);
        //наличие и переход по кнопке "Открыть счет" на странице Счет:
        const buttonNewAccounts = '#body > #contentbar > form > .form-actions > .btn';
        await page.waitForSelector(buttonNewAccounts);
        await page.click(buttonNewAccounts);
        const newAccountsHeader = await page.textContent('#inner-wrapper > #body > #contentbar > .page-header > h1');

        expect(newAccountsHeader).to.have.string('Открытие нового');
    });

    it('Главное меню: пользователь может перейти на вкладку Карты в адаптивной версии сайта', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await new MenuFragments().gotoPageCards(page);
        const cardsHeader = await page.textContent('#inner-wrapper > #body > #contentbar > .page-header > h1');

        expect(cardsHeader).to.have.string('Обзор карт');
    });

    it('Главное меню: пользователь может перейти на вкладку Вклады в адаптивной версии сайта', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await new MenuFragments().gotoPageDeposits(page);
        const buttonOpenDeposits = await page.textContent('#inner-wrapper > #body > #contentbar #btn-show-rates');

        expect(buttonOpenDeposits).to.have.string('Открыть вклад');
    });

    it('Главное меню: пользователь может перейти на вкладку Кредиты и ознакомиться с кредитными предложениями в адаптивной версии сайта', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await new MenuFragments().gotoPageLoans(page);
        //наличие и переход по кнопке "Ознакомиться с предложениями" на странице Кредиты:
        const buttonLoans = '#inner-wrapper > #body > #contentbar #loan-application-btn';
        await page.waitForSelector(buttonLoans);
        await page.click(buttonLoans);
        const formLoansHeader = await page.textContent('#inner-wrapper > #body > #contentbar > .page-header > h1');

        expect(formLoansHeader).to.have.string('Заявка на кредит');
    });

    it('Главное меню: пользователь может перейти на вкладку Валюта в адаптивной версии сайта', async () => {
        await app().AuthPage().login(page);
        await app().AuthPage().confirmAuth(page);
        await new MenuFragments().gotoPageCurrency(page);
        const currencyHeader = await page.textContent('#inner-wrapper > #body > #contentbar > .page-header > h1');

        expect(currencyHeader).to.have.string('Обмен валюты');
    });

});
