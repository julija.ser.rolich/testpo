const MenuFragments = function(){

    this.gotoPageOverview = async function (page){
        const overview = '.navbar #overview';
        await page.waitForSelector(overview);
        await page.click(overview);
    };

    this.gotoPageAccounts = async function (page){
        const accountsIndex = '.navbar #accounts > #accounts-index';
        await page.waitForSelector(accountsIndex);
        await page.click(accountsIndex);
    };

    // this.gotoPageAccountsStatement = async function (page){
    //     const accountsSt = '.navigation-menu > #accounts #statements-statement';
    //     await page.waitForSelector(accountsSt);
    //     await page.click(accountsSt);
    // };

    this.gotoPagePayments = async function (page){
        const paymentsPage = '.navbar #payments-form';
        await page.waitForSelector(paymentsPage);
        await page.click(paymentsPage);
    };

    this.gotoPageCards = async function (page){
        const cards = '.navbar #cards-overview-index';
        await page.waitForSelector(cards);
        await page.click(cards);
    };

    this.gotoPageDeposits  = async function (page){
        const deposits = '.navbar #deposits-index';
        await page.waitForSelector(deposits);
        await page.click(deposits);
    };

    this.gotoPageLoans = async function (page){
        const loans = '.navbar #loans-index';
        await page.waitForSelector(loans);
        await page.click(loans);
    };

    this.gotoPageCurrency = async function (page){
        const currency = '.navbar #externaltraderoom-index';
        await page.waitForSelector(currency);
        await page.click(currency);
    };


};
export { MenuFragments };
