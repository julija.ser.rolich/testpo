import { AuthPage } from "./authPage";
import { PaymentsPage } from "./paymentsPage";
import { CardsPage } from "./cardsPage";
import { Logout } from "./logout";

const app = () => ({
    AuthPage: () => new AuthPage(),
    PaymentsPage: () => new PaymentsPage(),
    CardsPage: () => new CardsPage(),
    Logout: () => new Logout(),
});

export { app };
