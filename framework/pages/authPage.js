const AuthPage = function(){
    const doublerLoginButton = 'body #login-otp-button';
    const passwordField = '.container > #login-container > #login-form > .form-control:nth-child(3) > input';
    const loginButton = '.container > #login-container > #login-form #login-button';
    const usernameField = '.container > #login-container > #login-form > .form-control:nth-child(2) > input';
    const forgetButton = '.container > #login-container > #login-form > #additional-actions > .chevron';


this.login = async function(page){
    await page.waitForSelector(usernameField);
    await page.click(usernameField);
    await page.fill(usernameField, '');
    await page.fill(usernameField, 'demo');
    await page.waitForSelector(passwordField);
    await page.click(passwordField);
    await page.fill(passwordField, '');
    await page.fill(passwordField, 'demo');
    await page.waitForSelector(loginButton);
    await page.click(loginButton);
};

this.confirmAuth = async function(page){
    await page.waitForSelector(doublerLoginButton);
    await page.click(doublerLoginButton);
};

this.forget = async function(page) {
    await  page.waitForSelector(forgetButton);
    await  page.click(forgetButton);
};


};
export { AuthPage };
