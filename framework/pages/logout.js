const Logout = function(){
    this.logout = async function(page){
        const closeIcon = '.content > #controls > #user > #logout-button > .icon-close';
        await page.waitForSelector(closeIcon);
        await page.click(closeIcon);
    };
};

export { Logout };
