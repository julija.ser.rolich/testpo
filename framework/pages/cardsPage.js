const CardsPage = function(){

    this.newCard = async function(page){
        const buttonOrderNewCard = '.span3 > #cards #order-new-card-link';
        await page.waitForSelector(buttonOrderNewCard);
        await page.click(buttonOrderNewCard);
    };
    this.openDropDownCards = async function(page){
        const dropDownCards = '#cards-vertical-container #type-select';
        await page.waitForSelector(dropDownCards);
        await page.click(dropDownCards);
    };
};

export { CardsPage }
