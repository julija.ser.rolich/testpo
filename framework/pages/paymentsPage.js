import { MenuFragments } from '../page fragments/menuFragments';

const PaymentsPage = function(){

    this.goPayments = async function(page){
        await new MenuFragments().gotoPagePayments(page);
    };

    this.history = async function(page){
        const history = ('#contentbar > #payments-left-menu > #dashboard-payment-type-menu > li > .history');
        await page.waitForSelector(history);
        await page.click(history);
    };

    this.dropDownAllBills = async function(page){
        const historyFilter = ('#contentbar > .tab-content > #history-form > #payment-history-filter > select');
        await page.waitForSelector(historyFilter);
        await page.click(historyFilter);
    };

    this.datePickerFrom = async function(page){
        const datePicker = ('.tab-content > #history-form > #payment-history-filter > #from-date > .input-small');
        await page.waitForSelector(datePicker);
        await page.click(datePicker);
    };

    this.buttonApply = async function(page){
        const buttonApply = ('#contentbar #apply-payments-filter');
        await page.waitForSelector(buttonApply);
        await page.click(buttonApply);
    };

    this.favorites = async function(page){
        const favorites = '#contentbar > #payments-left-menu > #dashboard-payment-type-menu > li > .favorites';
        await page.waitForSelector(favorites);
        await page.click(favorites);
    };

    this.transfers = async function(page){
        const transfers = '#contentbar > #payments-left-menu > #dashboard-payment-type-menu > li > .transfers';
        await page.waitForSelector(transfers);
        await page.click(transfers);
    };

    this.vendorPayments = async function(page){
        const vendorPayments = '#contentbar > #payments-left-menu > #dashboard-payment-type-menu > li > .vendor-payments';
        await page.waitForSelector(vendorPayments);
        await page.click(vendorPayments);
    };

    this.autoPayment = async function(page){
        const autoPayment = '#contentbar > #payments-left-menu > #dashboard-payment-type-menu > li > .auto-payment';
        await page.waitForSelector(autoPayment);
        await page.click(autoPayment);
    };

    this.requestMoney = async function(page){
        const requestMoney = '#contentbar > #payments-left-menu > #dashboard-payment-type-menu > li > .request-money';
        await page.waitForSelector(requestMoney);
        await page.click(requestMoney);
    };


};

export { PaymentsPage }
